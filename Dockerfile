FROM node:16-alpine

RUN mkdir /app
ADD server/build /app/server/build

# statstic-root ca for testing with vagrant box statastic.test
COPY statastic-rootCA.crt /usr/local/share/ca-certificates/statastic-rootCA.crt
ENV NODE_EXTRA_CA_CERTS=/usr/local/share/ca-certificates/statastic-rootCA.crt

# FIXME: copy only production-modules (no dev dependencies)
ADD server/node_modules /app/server/node_modules
ADD shared/build /app/shared
ADD client/dist/statastic /app/client

WORKDIR /app/server/build

CMD ["node", "index.js"]