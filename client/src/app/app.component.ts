import {Component, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "./reducers/app.reducer";


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

    constructor(private store: Store<AppState>) {
        this.store.select(state => state.datasourceState.datasourceKeys).subscribe(x => console.log(x));
    }

    ngOnInit(): void {
    }
}
