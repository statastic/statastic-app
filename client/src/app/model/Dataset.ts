
export interface Dataset {
    title: string;
    description: string;
    source: string;
    license: string;
    attribution: string;
    tags: { [key:string]: string }
}

export const EMPTY_DATASET: Dataset = {
    title: '',
    description: '',
    source: '',
    license: '',
    attribution: '',
    tags: {}
}
