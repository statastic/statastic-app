import {Dataset} from "./Dataset";

export interface Datasource {
    _id: string
    name: string;
    description: string;
    datasets: Dataset[]
}

export const EMPTY_DATASOURCE: Datasource = {
    _id: '',
    name: '',
    description: '',
    datasets: []
}
