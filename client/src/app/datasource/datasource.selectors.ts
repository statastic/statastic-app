import {createFeatureSelector, createSelector} from "@ngrx/store";
import {DatasourceState} from "./datasource.state";
import {tap} from "rxjs";

export const getDatasourceState = createFeatureSelector<DatasourceState>('datasourceState');

export const getSelectedDatasourceKey = createSelector(
    getDatasourceState,
    (state : DatasourceState) => state.selectedDatasourceKey);

export const getDatasourceKeys = createSelector(
    getDatasourceState,
    (state : DatasourceState) => state.datasourceKeys);

export const getDatasource = createSelector(
    getDatasourceState,
    (state : DatasourceState) => state.datasource);

export const getDataset = createSelector(
    getDatasourceState,
    (state : DatasourceState) => state.dataset);
