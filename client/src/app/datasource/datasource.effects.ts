import {Injectable} from "@angular/core";
import {DatasourceActionTypes, loadedDatasource, loadedDatasourceKeys} from "./datasource.actions";
import {catchError, EMPTY, filter, map, mergeMap, tap, withLatestFrom} from "rxjs";
import {DatasourceService} from "../services/datasource-service";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {Store} from "@ngrx/store";


@Injectable()
export class DatasourceEffects {

    constructor(
        private store: Store,
        private actions$: Actions,
        private datasourceService: DatasourceService
    ) {}

    loadDatasourceKeys$ = createEffect(() => this.actions$.pipe(
            ofType(DatasourceActionTypes.LOAD_DATASOURCE_KEYS),
            mergeMap(() => this.datasourceService.getDatasources()
                .pipe(
                    map(keys => (loadedDatasourceKeys(keys))),
                    catchError(() => EMPTY)
                )
            )
        )
    );

    loadDatasource$ = createEffect(() => this.actions$.pipe(
            ofType(DatasourceActionTypes.LOAD_DATASOURCE),
            mergeMap(({key}) => this.datasourceService.getDatasource(key)
                .pipe(
                    map(key => (loadedDatasource(key))),
                    catchError(() => EMPTY)
                )
            )
        )
    );

    // HINT: concatLatestFrom(() => this.store.select(...)),
}
