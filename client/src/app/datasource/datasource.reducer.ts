import {DATASOURCE_STATE_INITIAL, DatasourceState} from "./datasource.state";
import {createReducer, on} from "@ngrx/store";
import {loadedDatasource, loadedDatasourceKeys, selectDataset, selectTags} from "./datasource.actions";
import {Datasource} from "../model/Datasource";
import produce from "immer";

const updateDatasourceKeys = (state: DatasourceState, keys: string[]): DatasourceState => {
    return {
        ...state,
        datasourceKeys: keys
    };
}

const updateDatasource = (state: DatasourceState, datasource: Datasource): DatasourceState => {
    return {
        ...state,
        datasource: datasource
    };
}

const updateDataset = (state: DatasourceState, title: string): DatasourceState => {
    let dataset = state.datasource.datasets.find(ds => ds.title === title)
    if (!dataset) {
        return state;
    }
    console.log(dataset);
    return {
        ...state,
        dataset: dataset
    };
}

const updateSelectedTags = (state: DatasourceState, tags: string[]): DatasourceState => {
    if (tags) {
        tags = [...tags].sort();
    }
    state = produce(state, draftState => {
        return {
            ...draftState,
            selectedTags: tags
        }
    })
    return state;
}

export const datasourceReducer = createReducer(
    DATASOURCE_STATE_INITIAL,
    on(loadedDatasourceKeys, (state, {keys}) => updateDatasourceKeys(state, keys)),
    on(loadedDatasource, (state, {datasource}) => updateDatasource(state, datasource)),
    on(selectDataset, (state, {title}) => updateDataset(state, title)),
    on(selectTags, (state, {tags}) => updateSelectedTags(state, tags))
);
