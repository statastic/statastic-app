import {Datasource, EMPTY_DATASOURCE} from "../model/Datasource";
import {Dataset, EMPTY_DATASET} from "../model/Dataset";

export interface DatasourceState {
    datasourceKeys: string[],
    selectedDatasourceKey?: string,
    datasource: Datasource
    dataset: Dataset
    selectedTags: string[]
}

export const DATASOURCE_STATE_INITIAL: DatasourceState = {
    datasourceKeys: [],
    datasource: EMPTY_DATASOURCE,
    dataset: EMPTY_DATASET,
    selectedTags: []
}
