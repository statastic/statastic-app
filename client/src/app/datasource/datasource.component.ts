import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, Observable, Subscription, tap} from "rxjs";
import {Datasource} from "../model/Datasource";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Store} from "@ngrx/store";
import {AppState} from "../reducers/app.reducer";
import {loadDatasource, loadDatasourceKeys, selectDataset, selectTags} from "./datasource.actions";
import {getDataset, getDatasource, getDatasourceKeys} from "./datasource.selectors";
import {Dataset} from "../model/Dataset";

@Component({
  selector: 'app-datasource',
  templateUrl: './datasource.component.html',
  styleUrls: ['./datasource.component.scss']
})
export class DatasourceComponent implements OnInit, OnDestroy {

    datasources$: Observable<string[]>
    datasource$: Observable<Datasource>;
    dataset$: Observable<Dataset>;
    tags$: Observable<{tag: string, description:string}[]>
    form: FormGroup;

    private subscriptions = new Subscription();

    constructor(private store: Store<AppState>,
                private formBuilder: FormBuilder) {
        this.datasources$ = this.store.select(getDatasourceKeys);
        this.datasource$ = this.store.select(getDatasource);
        this.dataset$ = this.store.select(getDataset);
        this.tags$ = this.store.select(getDataset).pipe(map(ds => {
            let tags:{tag: string, description:string}[] = [];
            for (const [k, v] of Object.entries(ds.tags) ) {
                tags.push({tag: k, description: v });
            }
            return tags;
        }));

        this.form = this.formBuilder.group({
            datasource: '',
            dataset: '',
            tags: []
        });
        this.subscriptions.add(this.form.controls["datasource"].valueChanges
            .subscribe((e) => this.store.dispatch(loadDatasource(e))));
        this.subscriptions.add(this.form.controls["dataset"].valueChanges
            .subscribe((e) => this.store.dispatch(selectDataset(e))));
        this.subscriptions.add(this.form.controls["tags"].valueChanges
            .subscribe((e) => this.store.dispatch(selectTags(e))));
    }

    ngOnInit(): void {
        this.store.dispatch(loadDatasourceKeys());
    }

    ngOnDestroy(): void {
        this.subscriptions.unsubscribe();
    }
}
