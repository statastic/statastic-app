import { createAction } from '@ngrx/store';
import {Datasource} from "../model/Datasource";

export const enum DatasourceActionTypes {
    LOAD_DATASOURCE_KEYS = '[Datasource] Load Datasource Keys',
    LOADED_DATASOURCE_KEYS = '[Datasource] Loaded Datasource Keys',
    LOAD_DATASOURCE = '[Datasource] Load Datasource',
    LOADED_DATASOURCE = '[Datasource] Loaded Datasource',
    SELECT_DATASET = '[Datasource] Select Dataset',
    SELECT_TAGS =  '[Datasource] Select Tags',
}

export const loadDatasourceKeys = createAction(DatasourceActionTypes.LOAD_DATASOURCE_KEYS);

export const loadedDatasourceKeys = createAction(
    DatasourceActionTypes.LOADED_DATASOURCE_KEYS,
    (keys: string[]) => ({keys}),
);

export const loadDatasource = createAction(
    DatasourceActionTypes.LOAD_DATASOURCE,
    (key: string) => ({key}),
);

export const loadedDatasource = createAction(
    DatasourceActionTypes.LOADED_DATASOURCE,
    (datasource: Datasource) => ({datasource}),
);

export const selectDataset = createAction(
    DatasourceActionTypes.SELECT_DATASET,
    (title: string) => ({title}),
);

export const selectTags = createAction(
    DatasourceActionTypes.SELECT_TAGS,
    (tags: string[]) => ({tags}),
);
