import {ActionReducerMap, createReducer, on} from '@ngrx/store';
import {DatasourceState} from "../datasource/datasource.state";
import {datasourceReducer} from "../datasource/datasource.reducer";

export interface AppState {
    datasourceState: DatasourceState
}

export const reducers: ActionReducerMap<AppState> = {
    datasourceState: datasourceReducer
};
