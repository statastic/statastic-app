import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DatasourceService} from "./services/datasource-service";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {StoreModule} from '@ngrx/store';
import {DatasourceComponent} from './datasource/datasource.component';
import {datasourceReducer} from "./datasource/datasource.reducer";
import { EffectsModule } from '@ngrx/effects';
import {DatasourceEffects} from "./datasource/datasource.effects";
import {MatSelectModule} from "@angular/material/select";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {environment} from "../environments/environment";
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

@NgModule({
    declarations: [
        AppComponent,
        DatasourceComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        MatSelectModule,
        StoreModule.forRoot({datasourceState: datasourceReducer}),
        !environment.production ? StoreDevtoolsModule.instrument() : [],
        EffectsModule.forRoot([DatasourceEffects]),
        StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production })
    ],
    providers: [FormBuilder, DatasourceService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
