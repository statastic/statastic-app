import {Observable, of} from "rxjs";
import {Injectable} from "@angular/core";
import {Datasource} from "../model/Datasource";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable()
export class DatasourceService {

    constructor(private http: HttpClient) {
    }

    getDatasources(): Observable<string[]> {
        return this.http.get<string[]>(`${environment.url}/datasource`)
    }

    getDatasource(key: string): Observable<Datasource> {
        return this.http.get<Datasource>(`${environment.url}/datasource/${key}`)
    }
}
