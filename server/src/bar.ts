import {Baz} from '../../shared/src/baz'

let x : Baz = {
    bar: 1,
    foo: 'yyy'
};

console.log("bar" + x.bar);