
import {measurements} from "./flux.js";

import {datasourceList, datasource} from "./datasource";
import expressSession from "express-session";
import passport from "passport";
import {BaseClient, Issuer, Strategy} from "openid-client";


const express = require("express");
const cors = require('cors')

const ENVIRONMENT = {
    EXTRNAL_URL: process.env.EXTRNAL_URL || 'http://localhost:3001',
    PORT: process.env.PORT || '3001',
    OIDC_URL: process.env.OIDC_URL || 'https://keycloak.statastic.test/realms/statastic',
    OIDC_CLIENT_SECRET: process.env.OIDC_CLIENT_SECRET || 'xppKGPy5dYyyPVm7DSzItKFdwvJIOmtS'
}

const app = express();
app.use(cors())

var memoryStore = new expressSession.MemoryStore();
app.use(
    expressSession({
        secret: 'another_long_secret',
        resave: false,
        saveUninitialized: true,
        store: memoryStore
    })
);

const createClient = async () => {
    const keycloakIssuer = await Issuer.discover(ENVIRONMENT.OIDC_URL)

    const client = new keycloakIssuer.Client({
        client_id: 'statastic-app',
        client_secret: ENVIRONMENT.OIDC_CLIENT_SECRET,
        redirect_uris: [`${ENVIRONMENT.EXTRNAL_URL}/auth/callback`],
        post_logout_redirect_uris: [`${ENVIRONMENT.EXTRNAL_URL}/logout/callback`],
        response_types: ['code'],
    });
    return client;
}

const setupPassport = (client: BaseClient) => {
    // this creates the strategy
    passport.use('oidc', new Strategy({client}, (tokenSet: any, userinfo: any, done: any) => {
            return done(null, tokenSet.claims());
        })
    )

    passport.serializeUser(function (user: any, done: any) {
        done(null, user);
    });
    passport.deserializeUser(function (user: any, done: any) {
        done(null, user);
    });
}

(async () => {

    app.use(passport.initialize());
    app.use(passport.authenticate('session'));

    const client = await createClient();
    setupPassport(client);

    app.get('/authenticate', (req: any, res: any, next: any) => {
        passport.authenticate('oidc')(req, res, next);
    });

    app.get('/auth/callback', (req: any, res: any, next: any) => {
        passport.authenticate('oidc', {
            successRedirect: '/secured',
            failureRedirect: '/'
        })(req, res, next);
    });

    var checkAuthenticated = (req: any, res: any, next: any) => {
        if (req.isAuthenticated()) {
            return next()
        }
        res.redirect("/authenticate")
    }

    app.get('/secured', checkAuthenticated, (req: any, res: any) => {
        res.json({message: "Secured!"});
    });

    app.get('/other', checkAuthenticated, (req: any, res: any) => {
        res.json({message: "Other!"});
    });

    app.get('/logout', (req: any, res: any) => {
        res.redirect(client.endSessionUrl());
    });
    app.get('/logout/callback', (req: any, res: any) => {
        req.logout(()=>{});
        res.redirect('/');
    });

    app.get('/', datasourceList);
    app.get('/measurements', measurements);
    app.get('/datasource', datasourceList);
    app.get('/datasource/:key', datasource);

    app.listen(ENVIRONMENT.PORT, () => {
        console.log(`Server listening on ${ENVIRONMENT.PORT}`);
    });

    app.use('/client', express.static('../../client'))
})()