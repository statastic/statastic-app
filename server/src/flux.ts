import fetch from "cross-fetch";
const papa = require("papaparse");

const measurements = async (req: any, res: any) => {
    let x = await fetch('https://influxdb.statastic.io/api/v2/query?org=statastic',
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/vnd.flux',
                'Authorization': `Token ${process.env.FLUX_API_TOKEN}`,
                'Accept': 'application/csv'
            },
            body: 'import "influxdata/influxdb/schema"\r\n' +
                'schema.measurements(bucket: "statastic-bucket")'
        });

    let t = await x.text()

    let json = papa.parse(t, {header: true, skipEmptyLines: true});
    let result = json.data.map((x: any) => x._value)
    result.push("asdf", "xyz")
    res.send(result);
};


export {measurements};