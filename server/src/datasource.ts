import fetch from "cross-fetch";

export const datasourceList = async (req: any, res: any) => {
    let response = await fetch('https://couchdb.statastic.io/readonly/statastic/_all_docs',);

    let responseDoc = await response.json();

    let docs = responseDoc.rows.map((d: any) => d.key);
    res.send(docs);
};

export const datasource = async (req: any, res: any) => {
    let response = await fetch(`https://couchdb.statastic.io/readonly/statastic/${req.params.key}`,);

    let responseDoc = await response.json();
    res.send(responseDoc);
};

export const data = async (req: any, res: any) => {
    let response = await fetch(`https://couchdb.statastic.io/readonly/statastic/${req.params.key}`,);

    let responseDoc = await response.json();
    res.send(responseDoc);
};
